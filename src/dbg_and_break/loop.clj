(ns dbg-and-break.loop)

(loop [i 1]
  (when (<= i 5)
     #break
    (println i)
    (recur (inc i))))
